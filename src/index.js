import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import { Provider } from 'react-redux';
import { store } from './store';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

// if(window.navigator && navigator.serviceWorker) {
//   navigator.serviceWorker.getRegistrations()
//   .then(function(registrations) {
//     for(let registration of registrations) {
//       registration.unregister();
//     }
//   });
// }

class AppWrapper extends React.Component {
  constructor() {
    super();
  }
  render() {
    return (
      <MuiThemeProvider muiTheme={getMuiTheme()}>
        <App />
      </MuiThemeProvider>
    )
  }
}

ReactDOM.render(
<Provider store={store}>
  <AppWrapper />
</Provider>, document.getElementById('root'));
registerServiceWorker();
//
//unregister();
