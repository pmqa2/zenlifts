import reducer from './reducer';
import { createStore, applyMiddleware } from 'redux';
import dotProp from 'dot-prop-immutable';

const customMiddleWare = store => next => action => {
  next(action);
}


export const store = createStore(reducer, applyMiddleware(customMiddleWare));
