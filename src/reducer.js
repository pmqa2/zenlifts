import dotProp from 'dot-prop-immutable';

import {
  UPDATE_POST_TITLE,
  LOAD_APP,
  SET_WORKOUT,
  GO_TO_PAGE,
  SET_WEIGHT,
  SET_SETS,
  SET_REPS,
  SET_NAME,
  SET_WEIGHT_LIFTS,
  RESET_TIMER,
  UPDATE_SET_COMPLETE,
  UPDATE_EXERCISE_COMPLETE,
  CANCEL_WORKOUT,
  SETUP_WORKOUT,
  ADD_WORKOUT,
  STOP_TIMER,
  ADD_EXERCISE,
  DELETE_EXERCISE,
  DELETE_EXERCISE_COMPLETELY,
  SET_CURRENT_TIME
} from './constants/actionTypes';

export default (state = {}, action) => {
  let nextState = {};
  switch (action.type) {
    case LOAD_APP:
      return action.state
    case SET_WORKOUT:
      return {
        ...state,
        currentWorkout: action.payload,
        page: 'workout'
      }
    case GO_TO_PAGE:
      return {
        ...state,
        page: action.payload
      }
    case SET_WEIGHT:
      nextState = dotProp.set(state, `exercises.${action.id}.weight`, action.weight);
      return nextState
    case SET_SETS:
      nextState = dotProp.set(state, `exercises.${action.id}.sets`, action.sets);
      return nextState
    case SET_REPS:
      nextState = dotProp.set(state, `exercises.${action.id}.reps`, action.reps);
      return nextState
    case SET_NAME:
      nextState = dotProp.set(state, `exercises.${action.id}.title`, action.name);
      console.log(nextState);
      return nextState
    case RESET_TIMER:
      nextState = dotProp.set(state, 'timer.resetTimer', action.date);
      return nextState
    case UPDATE_SET_COMPLETE:
      nextState = dotProp.set(state, 'currentWorkout', action.obj);
      return nextState
    case CANCEL_WORKOUT:
      nextState = dotProp.set(state, 'currentWorkout', false);
      nextState.page = "home";
      return nextState
    case SETUP_WORKOUT:
      nextState = Object.assign({}, state);
      nextState.exercises = action.exercisesNext;
      nextState.workoutHistory = action.workoutHistoryNext;
      nextState.page = "home";
      nextState.currentWorkout = false;
      return nextState
    case ADD_WORKOUT:
      nextState = Object.assign({}, state);
      nextState.workout.push(action.workout);
      return nextState;
    case SET_WEIGHT_LIFTS:
      nextState = Object.assign({}, state);
      (action.id, action.weight);
      nextState.currentWorkout.exercises = nextState.currentWorkout.exercises.map((exercise) => {
        if (exercise.id === action.id) {
          return Object.assign({}, exercise, {
            weight: action.weight
          })
        } else return exercise;
      });
      (nextState);
      return nextState
    case STOP_TIMER:
      nextState = dotProp.set(state, 'timer.resetTimer', false);
      return nextState
    case ADD_EXERCISE:
      nextState = dotProp.set(state, `exercises.${action.exercise.id}`, action.exercise);
      nextState.workout = nextState.workout.map((workout) => {
        if (workout.id === action.workoutId) {
          workout.exercises.push({id: action.exercise.id});
          console.log(workout);
          return workout;
        } else {
          return workout;
        }
      });
      console.log(nextState);
      return nextState
    case DELETE_EXERCISE:
      nextState = Object.assign({}, state);
      //nextState = dotProp.delete(nextState, `exercises.${action.id}`);
      nextState.workout = nextState.workout.map((workout) => {
        if (workout.id === action.workoutId) {
          let newExercises = workout.exercises.filter((exercise) => {
            if (exercise.id !== action.id) return exercise;
          });
          workout.exercises = newExercises;
          return workout;
        } else {
          return workout;
        }
      });
      return nextState
    case DELETE_EXERCISE_COMPLETELY:
      nextState = Object.assign({}, state);
      nextState = dotProp.delete(nextState, `exercises.${action.id}`);
      return nextState
    case SET_CURRENT_TIME:
      nextState = Object.assign({}, state);
      console.log(action);
      nextState = dotProp.set(nextState, `stateTime`, action.now);
      return nextState
    default:
      return state;
  }
};
