const config = {};

// setup api URL
if (process.env.REACT_APP_API) {
  config.api = process.env.REACT_APP_API;
  console.log(process.env);
}
else
  config.api = 'https://backend-zenlifts.herokuapp.com/backend';


// dataToken string

if (process.env.REACT_APP_DATA_TOKEN_NAME)
  config.dataToken = process.env.REACT_APP_DATA_TOKEN_NAME;
else
  config.dataToken = 'zenliftsDataDev';

// setup api URL
if (process.env.REACT_APP_CONNECTION_TOKEN_NAME)
  config.connectionToken = process.env.REACT_APP_CONNECTION_TOKEN_NAME;
else
  config.connectionToken = 'zenliftsTokenDev';


export default config;
