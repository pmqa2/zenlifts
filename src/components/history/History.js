import React from 'react';
import { connect } from 'react-redux';
import { SET_WORKOUT, GO_TO_PAGE, LOAD_APP } from '../.././constants/actionTypes';
import { store } from '../../store';
import Auth from '../../auth/auth';
import { requests } from '../../agent';
import EditWeightsLift from '../elements/EditWeightsLifts';
import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn,
} from 'material-ui/Table';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import State from '../../workouts/State';

const mapStateToProps = state => ({ ...state });

const mapDispatchToProps = dispatch => ({
  loadApp: (state) => {
    dispatch({ type: LOAD_APP, state })
  }
});

class HistoryLifts extends React.Component {
  constructor() {
    super();
  }
  componentWillMount() {
  }
  render() {
    ('rendered');
    const style = {
      display: 'inline-block',
      width: 50,
      minWidth: 50,
      margin: 5,
      textAlign: 'center'
    };
    let styleTd = {fontSize: '10px', overflow: 'initial', paddingLeft: '5px', paddingRight: '5px'};

    function formatDate(d) {
      var date = new Date(d);
      var monthNames = [
        "Jan", "Feb", "Mar",
        "Apr", "May", "Jun", "Jul",
        "Aug", "Sep", "Oct",
        "Nov", "Dec"
      ];

      var day = date.getDate();
      var monthIndex = date.getMonth();
      var year = date.getFullYear();

      return day + ' ' + monthNames[monthIndex];
    }

    if (this.props.historyWorkoutId) {
      let exercise = this.props.exercises[this.props.historyWorkoutId];
      let currentWorkoutExercise = this.props.currentWorkout.exercises
          .filter(ex => ex.id === this.props.historyWorkoutId);
      (this.props);
      let exerciseHistory = exercise.history.map((ex, index) => {
        if (1) {
          let variation = 0;
          if (exercise.history[index+1] !== undefined) {
            if (exercise.history[index].volume == 0) {
              variation = 0;
            } else {
              variation = (exercise.history[index].volume - exercise.history[index+1].volume)/exercise.history[index].volume;
              variation = 100*variation;
              variation = variation.toFixed(2);
            }
          }
          let reps = ex.hasOwnProperty('finishedReps') ? ex.finishedReps : 0;
          return (
            <TableRow key={index}>
              <TableRowColumn style={styleTd}>{formatDate(ex.date)}</TableRowColumn>
              <TableRowColumn style={styleTd}>{ex.weight}</TableRowColumn>
              <TableRowColumn style={styleTd}>{reps}</TableRowColumn>
              <TableRowColumn style={styleTd}>{reps*ex.weight}</TableRowColumn>
              <TableRowColumn style={styleTd}>{variation + '%'}</TableRowColumn>
            </TableRow>
          );
        }
      });
      return (
        <div className="history">
          <div style={{ textAlign: 'center'}}>
            <EditWeightsLift
              exerciseId = {this.props.historyWorkoutId}
              exerciseInfo = {currentWorkoutExercise[0]}
              increment={exercise.increment} />
          </div>
          <div className="text-center home">
            <h2>History</h2>
          </div>
          <Table selectable = {false}>
            <TableHeader displaySelectAll={false} enableSelectAll={false} style={{display: 'contents'}}>
              <TableRow style={{fontSize: '11px'}}>
                <TableHeaderColumn style={styleTd}>Date</TableHeaderColumn>
                <TableHeaderColumn style={styleTd}>Weight</TableHeaderColumn>
                <TableHeaderColumn style={styleTd}>Reps</TableHeaderColumn>
                <TableHeaderColumn style={styleTd}>Volume</TableHeaderColumn>
                <TableHeaderColumn style={styleTd}>Variation</TableHeaderColumn>
              </TableRow>
            </TableHeader>
            <TableBody displayRowCheckbox={false} >
              {exerciseHistory}
            </TableBody>
          </Table>
        </div>
      )
    } else return null
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(HistoryLifts);
