import React from 'react';
import logo from '../../logo.svg';
import { GO_TO_PAGE } from '../../constants/actionTypes';
import { connect } from 'react-redux';

const mapDispatchToProps = dispatch => ({
  goToPage: (payload) =>
    dispatch({ type: GO_TO_PAGE, payload })
});

class Header extends React.Component {
  constructor() {
    super();
    this.handleGoHomeClick = this.handleGoHomeClick.bind(this);
  }
  handleGoHomeClick(){
    this.props.goToPage('home');
  }
  render() {
    return (
      <div className="header">
        <div onClick={this.handleGoHomeClick} className="go-back">&lt;</div>
        <p className="text-center menu">{this.props.pageTitle}</p>
      </div>
    )
  }
}

export default connect(null, mapDispatchToProps)(Header);
