import React from 'react';
import { SET_REPS } from '../../constants/actionTypes';
import { connect } from 'react-redux';

const mapDispatchToProps = dispatch => ({
  onChangeReps: (reps, id) =>
    dispatch({ type: SET_REPS, reps, id })
});

class EditReps extends React.Component {
  constructor() {
    super();
  }
  onChangeReps(e) {
    let num = Number(e.target.value);
    this.props.onChangeReps(num, this.props.exerciseInfo.id);
  }
  render() {
    return(
      <input placeholder={this.props.exerciseInfo.reps} onChange={this.onChangeReps.bind(this)} />
    )
  }
}

export default connect(null, mapDispatchToProps)(EditReps);
