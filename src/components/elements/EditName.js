import React from 'react';
import { SET_REPS } from '../../constants/actionTypes';
import { connect } from 'react-redux';

const mapDispatchToProps = dispatch => ({
  onChangeName: (reps, id) =>
    dispatch({ type: SET_NAME, name, id })
});

class EditName extends React.Component {
  constructor() {
    super();
  }
  onChangeReps(e) {
    let num = Number(e.target.value);
    this.props.onChangeName(e.target.value, this.props.exerciseInfo.id);
  }
  render() {
    return(
      <input placeholder={this.props.exerciseInfo.reps} onChange={this.onChangeName.bind(this)} />
    )
  }
}

export default connect(null, mapDispatchToProps)(EditReps);
