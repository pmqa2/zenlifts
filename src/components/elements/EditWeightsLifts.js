import React from 'react';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import { SET_WEIGHT_LIFTS } from '../../constants/actionTypes';
import { connect } from 'react-redux';

const mapStateToProps = state => ({ ...state });

const mapDispatchToProps = dispatch => ({
  onChangeWeight: (weight, id) =>
    dispatch({ type: SET_WEIGHT_LIFTS, weight, id })
});


class EditWeightsLift extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      weight: this.props.exerciseInfo.weight
    }

    this.decrementWeight = this.decrementWeight.bind(this);
    this.incrementWeight = this.incrementWeight.bind(this);
    this.editWeight = this.editWeight.bind(this);
    this.editWeightState = this.editWeightState.bind(this);
    this.cleanNumber = this.cleanNumber.bind(this);

  }
  cleanNumber(n, typey = 'integer') {
    if (typeof(n) === 'number') {
      n = Math.abs(Number(n));
    } else {
      n = n.replace(/[^0-9.]/g,"");
      n = Math.abs(Number(n));
    };

    console.log(typeof(n));
    console.log(n);

    // if (typey === 'partial') {
    //   console.log(n);
    //   n = n.toFixed(2);
    // } else if (typey === 'integer') {
    //   n = Math.floor(n);
    // };

    return n;
  }
  decrementWeight(w) {
    let num = w - this.props.increment;
    this.props.onChangeWeight(num, this.props.exerciseInfo.id);
    this.forceUpdate()
  }
  incrementWeight(w) {
    let num = w + this.props.increment;
    this.props.onChangeWeight(num, this.props.exerciseInfo.id);
    this.forceUpdate();
  }
  editWeight(e) {
    let num = this.cleanNumber(this.state.weight);
    console.log(num);
    this.props.onChangeWeight(num, this.props.exerciseInfo.id);
    this.setState({weight: num});
  }
  editWeightState(e) {
    console.log(e.target.value);
    this.setState({weight: e.target.value});
  }
  render() {
    console.log(this.state.weight);
    const style = {
      display: 'inline-block',
      width: 50,
      minWidth: 50,
      margin: 5,
      textAlign: 'center'
    };

    this.currentWorkoutExercise = null;

    (this.props);

    for (var i = 0; i < this.props.currentWorkout.exercises.length; i++) {
      if (this.props.currentWorkout.exercises[i].id == this.props.exerciseId) {
        this.currentWorkoutExercise = this.props.currentWorkout.exercises[i];
      }
    }

    (this.props.currentWorkout);

    return (
      <div>
        <h3>{this.currentWorkoutExercise.title}</h3>
        <RaisedButton onClick={() => this.decrementWeight(this.currentWorkoutExercise.weight)} label="-" style={style} primary/>
        <TextField
          inputStyle={{ textAlign: 'center' }}
          value={this.state.weight}
          style={style}
          onBlur={this.editWeight}
          onChange={this.editWeightState}
        />
        <RaisedButton onClick={() => this.incrementWeight(this.currentWorkoutExercise.weight)} label="+" style={style} primary/>
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(EditWeightsLift);
