import React from 'react';
import { SET_SETS } from '../../constants/actionTypes';
import { connect } from 'react-redux';

const mapDispatchToProps = dispatch => ({
  onChangeSets: (sets, id) =>
    dispatch({ type: SET_SETS, sets, id })
});

class EditSets extends React.Component {
  constructor() {
    super();
  }
  onChangeSets(e) {
    let num = Number(e.target.value);
    this.props.onChangeSets(num, this.props.exerciseInfo.id);
  }
  render() {
    (this.props);
    return(
      <input placeholder={this.props.exerciseInfo.sets} onChange={this.onChangeSets.bind(this)} />
    )
  }
}

export default connect(null, mapDispatchToProps)(EditSets);
