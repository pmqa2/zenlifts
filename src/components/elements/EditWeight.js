import React from 'react';
import { SET_WEIGHT } from '../../constants/actionTypes';
import { connect } from 'react-redux';

const mapDispatchToProps = dispatch => ({
  onChangeWeight: (weight, id) =>
    dispatch({ type: SET_WEIGHT, weight, id })
});

class EditWeight extends React.Component {
  constructor() {
    super();
    this.state = {};
    this.onChangeWeight = this.onChangeWeight.bind(this);
  }
  onChangeWeight(e) {
    let num = Number(e.target.value);
    this.props.onChangeWeight(num, this.props.exerciseInfo.id);
  }
  render() {
    return(
      <input placeholder={this.props.exerciseInfo.weight} onChange={this.onChangeWeight} />
    )
  }
}

export default connect(null, mapDispatchToProps)(EditWeight);
