import React from 'react';
import { SET_WEIGHT_LIFTS } from '../../constants/actionTypes';
import { connect } from 'react-redux';

const mapDispatchToProps = dispatch => ({
  onChangeWeightLifts: (weight, id) =>
    dispatch({ type: SET_WEIGHT_LIFTS, weight, id })
});

class EditWeightLift extends React.Component {
  constructor() {
    super();
    this.state = {};
    this.onChangeWeight = this.onChangeWeight.bind(this);
  }
  onChangeWeight(e) {
    let num = Number(e.target.value);
    this.props.onChangeWeightLifts(num, this.props.id);
  }
  render() {
    return(
      <input placeholder={this.props.weight} onChange={this.onChangeWeight.bind(this)} />
    )
  }
}

export default connect(null, mapDispatchToProps)(EditWeightLift);
