import React from 'react';
import logo from '../../logo.svg';
import { GO_TO_PAGE } from '../../constants/actionTypes';
import { connect } from 'react-redux';

const mapDispatchToProps = dispatch => ({
  goToPage: (payload) =>
    dispatch({ type: GO_TO_PAGE, payload })
});

class Logo extends React.Component {
  constructor() {
    super();
    this.handleGoHomeClick = this.handleGoHomeClick.bind(this);
  }
  handleGoHomeClick(){
    this.props.goToPage('home');
  }
  render() {
    return (
      <div className="text-center logo">
        <a href="#">
          <h1 className="website-title text-center">Zenlifts</h1>
        </a>
      </div>
    )
  }
}

export default connect(null, mapDispatchToProps)(Logo);
