import React from 'react';
import { connect } from 'react-redux';
import { STOP_TIMER } from '../.././constants/actionTypes';

const mapDispatchToProps = dispatch => ({
  stopTimer: () =>
    dispatch({ type: STOP_TIMER }),
});

class LiftTimer extends React.Component {
  constructor() {
    super();
    this.state = {
      time: '0:0:0',
    };
    this.timerID = false;
    this.timeString = false;
    this.countTimer = this.countTimer.bind(this);
  }
  componentDidUpdate() {
    clearInterval(this.timerID);
    if (this.props.timer.resetTimer) {
      this.timerID = setInterval(this.countTimer, 50);
    }
  }
  componentWillUnmount() {
    clearInterval(this.timerID);
  }
  countTimer() {
    let elapsedSeconds = Date.now() - this.props.timer.resetTimer;
    this.setState({
      time: ms2Time(elapsedSeconds)
    });

    function ms2Time(ms) {
      var secs = ms / 1000;
      var minutes = secs / 60;
      secs = Math.floor(secs % 60);
      var hours = minutes / 60;
      minutes = Math.floor(minutes % 60);
      hours = Math.floor(hours % 24);
      return hours + ":" + minutes + ":" + secs ;
    }
  }
  closeTimer() {
    ('click');
    clearInterval(this.timerID);
    this.props.stopTimer();
  }
  render() {
    if (this.props.timer.resetTimer) {
      return(
        <div className="timer">
          <div className="timer-container">
            <div className="timer-message">
              <span>Good work. Rest for 1:30min.</span>
            </div>
            <div className="timer-label">
              <span>Timer:</span>
              <span>{this.state.time}</span>
            </div>
            <span onClick={this.closeTimer.bind(this)} className="timer-close">X</span>
          </div>
        </div>
      )
    } else {
      return null
    }
  }
}
export default connect(null, mapDispatchToProps)(LiftTimer);
