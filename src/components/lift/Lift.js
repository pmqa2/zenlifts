import React from 'react';
import LiftWorkout from './LiftWorkout';
import LiftTimer from './LiftTimer';
import Logo from '../elements/Logo';
import { connect } from 'react-redux';
import { CANCEL_WORKOUT, SETUP_WORKOUT, LOAD_APP } from '../.././constants/actionTypes';
import { requests, helper } from '../../agent';
import RaisedButton from 'material-ui/RaisedButton';
import Config from '../../config';

const mapStateToProps = state => ({ ...state });

const mapDispatchToProps = dispatch => ({
  cancelWorkout: () =>
    dispatch({ type: CANCEL_WORKOUT }),
  setupWorkout: (exercisesNext, workoutHistoryNext) =>
    dispatch({ type: SETUP_WORKOUT, exercisesNext, workoutHistoryNext }),
  onLoad: (state) => {
    dispatch({ type: LOAD_APP, state })
  }
});

class Lift extends React.Component {
  constructor() {
    super();
    this.state = {
      historyWorkoutId: false
    }
    this.submitExercises = this.submitExercises.bind(this);
    this.saveStateToServer = this.saveStateToServer.bind(this);
    this.downloadDataFromServer = this.downloadDataFromServer.bind(this);
  }
  componentWillMount() {
    //this.downloadDataFromServer();
  }
  componentDidUpdate() {
    helper.saveOffline(this.props);
  }
  downloadDataFromServer() {
    requests.get('api/data').then((res, err) => {
      if (err) return null;
      res.data.page = 'workout';
      this.props.onLoad(res.data);
    });
  }
  saveStateToServer() {
    if (this.props.page !== 'login') {
      helper.saveOnAPI(this.props);
    }
  }
  cancelWorkout(e) {
    e.preventDefault();
    this.props.cancelWorkout();
  }
  handleSubmitWorkoutButton(e) {
    e.preventDefault();
    this.submitExercises();
  }
  submitExercises() {
    let exercisesNext = Object.assign({}, this.props.exercises);
    this.props.currentWorkout.exercises.map((exercise) => {

      if (exercise.completed) {
        exercisesNext[exercise.id].weight = exercise.weight + exercisesNext[exercise.id].increment;
      } else exercisesNext[exercise.id].weight = exercise.weight;

      const finishedReps = (function(){
        let total = 0;
        for (let j = 0; j < exercise.sets.length; j++) {
          if (exercise.sets[j].clickedReps !== false)
            total = total + exercise.sets[j].clickedReps;
        }
        return total;
      })();

      if (finishedReps > 0) {
        //Set history
        exercisesNext[exercise.id].history.unshift({ weight: exercise.weight, date: Date.now(), finishedReps: finishedReps, volume: finishedReps*exercise.weight});
      }

    });

    let workoutHistoryNext = this.props.workoutHistory;
    let cw = Object.assign({}, this.props.currentWorkout);

    workoutHistoryNext.push(cw);

    this.props.setupWorkout(exercisesNext, workoutHistoryNext);
  }
  render() {
    (this.props.currentWorkout);
    return (
      <div>
        <LiftWorkout
          workout = {this.props.currentWorkout}
          handleSetButtonClick = {this.handleSetButtonClick}
          handleSetUpdate = {this.handleSetUpdate}
        />
        <LiftTimer
          timer = {this.props.timer}
        />
        <form className="text-center">
          <RaisedButton type="submit" label="Cancel Workout" primary onClick={(e) => this.cancelWorkout(e)} />
          <RaisedButton type="submit" label="Finish Workout" primary onClick={(e) => this.handleSubmitWorkoutButton(e)} />
        </form>
      </div>
    )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Lift);
