import React from 'react';
import LiftWorkoutExercise from './LiftWorkoutExercise';

class LiftWorkout extends React.Component {
  constructor() {
    super();
  }
  render() {
    return(
      <div className="lift-table">
        <SetsGroup
          exercises = {this.props.workout.exercises}
          handleSetUpdate = {this.props.handleSetUpdate}
        />
      </div >
    )
  }
}
class SetsGroup extends React.Component {
  constructor() {
    super();
  }
  render() {
    const Exercises = this.props.exercises.map((exercise, index) => {
      return (
        <LiftWorkoutExercise
          key = {index}
          exercise = {exercise}
          handleSetUpdate = {this.props.handleSetUpdate}
          workoutId = {exercise.id}
        />
      )
    });
    return (
      <div>
      {Exercises}
      </div>
    );
  }
}

export default LiftWorkout;
