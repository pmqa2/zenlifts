import React from 'react';
import { connect } from 'react-redux';
import { RESET_TIMER, UPDATE_SET_COMPLETE } from '../.././constants/actionTypes';

const mapStateToProps = state => ({ ...state });

const mapDispatchToProps = dispatch => ({
  handleSetButtonClick: (date) =>
    dispatch({ type: RESET_TIMER, date }),
  updateCompletedSetState: (obj) =>
    dispatch({ type: UPDATE_SET_COMPLETE, obj })
});

class LiftWorkoutExerciseSets extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      totalReps: this.props.set.totalReps,
      clickedReps: this.props.set.clickedReps
    };
    this.handleSetButtonClick = this.handleSetButtonClick.bind(this);
    this.handleSetUpdate = this.handleSetUpdate.bind(this);
    this.updateCompletedSetState = this.updateCompletedSetState.bind(this);
  }
  updateCompletedSetState(val, setId, exerciseId) {
    let currentWorkout = Object.assign({}, this.props.currentWorkout);
    currentWorkout.exercises = currentWorkout.exercises.map((exercise) => {
      if (exercise.id === exerciseId) {
        return Object.assign({}, exercise, {
          sets: exercise.sets.map((set) => {
            if (set.id == setId) {
              return Object.assign({}, set, { completed: val, clickedReps: this.state.clickedReps });
            } else return set;
          })
        });
      } else return exercise;
    });

    this.updateCompletedExerciseState(currentWorkout);
    // Send currentworkout object to reducer.js
    // this.setState({
    //   currentWorkout: currentWorkout
    // }, this.handleExerciseSetsUpdate);
  }
  updateCompletedExerciseState(currentWorkout) {
    let currentWorkoutNext = Object.assign({}, currentWorkout,
      { exercises: currentWorkout.exercises.map(ex => {
        let val = 0;

        ex.sets.map((set) => {
          if (set.completed === true) {
            val++;
          }
        });

        return Object.assign({}, ex, { completed: (val === ex.sets.length) ? true : false })
    })});

    (currentWorkoutNext);

    this.props.updateCompletedSetState(currentWorkoutNext);
    // this.setState({
    //   currentWorkout: currentWorkoutNext
    // });
  }
  handleSetUpdate(id) {
    if (this.state.clickedReps === this.state.totalReps) {
      this.updateCompletedSetState(true, id, this.props.workoutId);
    } else this.updateCompletedSetState(false, id, this.props.workoutId);
  }
  handleSetButtonClick(id) {
    if (this.state.clickedReps === false)
      this.setState({clickedReps: this.props.set.totalReps}, () => this.handleSetUpdate(id));
    else if (this.state.clickedReps === 0)
      this.setState({clickedReps: false}, () => this.handleSetUpdate(id));
    else
      this.setState({clickedReps: this.state.clickedReps-1}, () => this.handleSetUpdate(id));
    this.props.handleSetButtonClick(Date.now());
  }
  render() {
    //(this.state);
    //(this.props.clickedReps);
    if (this.state.clickedReps === false) {
      return(
        <button onClick={() => this.handleSetButtonClick(this.props.set.id)}>
          {this.state.totalReps}
        </button>
      )
    }
    else if (this.state.clickedReps < this.state.totalReps) {
      return(
        <button className='not-active' onClick={() => this.handleSetButtonClick(this.props.set.id)}>
          {this.state.clickedReps}
        </button>
      )
    }
    else {
      return(
        <button className='active' onClick={() => this.handleSetButtonClick(this.props.set.id)}>
          {this.state.clickedReps}
        </button>
      )
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(LiftWorkoutExerciseSets);
