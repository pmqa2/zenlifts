import React from 'react';
import LiftWorkoutExerciseSets from './LiftWorkoutExerciseSets';
import HistoryLifts from '../history/History';
import Dialog from 'material-ui/Dialog';
import Card from 'material-ui/Card';
import { connect } from 'react-redux';
import Typography from '@material-ui/core/Typography';

const mapStateToProps = state => ({ ...state });

class LiftWorkoutExercise extends React.Component {
  constructor() {
    super();
    this.state = {
      open: false
    }
    this.handleClick = this.handleClick.bind(this);
    this.handleRequestClose = this.handleRequestClose.bind(this);
  }
  handleClick = (event) => {
    // This prevents ghost click.
    event.preventDefault();

    this.setState({
      open: true,
      anchorEl: event.currentTarget,
    });
  };
  handleRequestClose = () => {
    this.setState({
      open: false
    });
  };
  render() {
    const currentWorkoutExercise = this.props.currentWorkout.exercises.filter(ex => ex.id === this.props.exercise.id);

    const sets = this.props.exercise.sets.map(set => {
      return (
        <LiftWorkoutExerciseSets
          key = {set.id}
          set = {set}
          handleSetButtonClick = {this.props.handleSetButtonClick}
          handleSetUpdate = {this.props.handleSetUpdate}
          workoutId = {this.props.workoutId}
        />
      )
    });
    return (
      <Card className="lift-workout-exercise">
        <Dialog
          title={false}
          modal={false}
          open={this.state.open}
          autoScrollBodyContent={true}
          onRequestClose={this.handleRequestClose}
        >
          <HistoryLifts
            historyWorkoutId={this.props.exercise.id}
          />
        </Dialog>
        <div className="lift-exercise-headers">
          <div className="lift-exercise-name">
            <h2>{currentWorkoutExercise[0].title}</h2>
          </div>
          <div onClick={this.handleClick.bind(this)} className="weight-edit-lift">
            <span>{currentWorkoutExercise[0].weight} kg</span>
          </div>
        </div>
        <div className="lift-exercise-sets">
          <div>
            {sets}
          </div>
        </div>
      </Card>
    )
  }
}

export default connect(mapStateToProps, null)(LiftWorkoutExercise);
