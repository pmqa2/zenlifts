import React from 'react';
import { connect } from 'react-redux';
import { store } from '../../store';
import EditExerciseDialogue from './EditExerciseDialogue';
import Dialog from 'material-ui/Dialog';
import { ADD_EXERCISE } from '../../constants/actionTypes';
import { SET_WEIGHT, SET_NAME, SET_SETS, SET_REPS, DELETE_EXERCISE } from '../../constants/actionTypes';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import dotProp from 'dot-prop-immutable';
import AutoComplete from 'material-ui/AutoComplete';

const mapDispatchToProps = dispatch => ({
  onChangeWeight: (weight, id) =>
    dispatch({ type: SET_WEIGHT, weight, id }),
  onChangeExerciseName: (name, id) =>
    dispatch({ type: SET_NAME, name, id }),
  onChangeSets: (sets, id) =>
    dispatch({ type: SET_SETS, sets, id }),
  onChangeReps: (reps, id) =>
    dispatch({ type: SET_REPS, reps, id }),
  deleteExercise: (id, workoutId) =>
    dispatch({ type: DELETE_EXERCISE, id, workoutId }),
  addExercise: (exercise, workoutId) =>
    dispatch({ type: ADD_EXERCISE, exercise, workoutId })
});


class EditRoutineWorkoutAddExercise extends React.Component {
  constructor() {
    super();
    this.stateRedux = store.getState();

    let maxId = 0;
    for (let id in this.stateRedux.exercises) {
      if (Number(id) > maxId) maxId = id;
    }
    maxId++;

    let exerciseInfo = {
      title: 'Name of Exercise',
      id: maxId,
      sets: 3,
      reps: 3,
      history: [],
      increment: 2.5,
      weight: 10
    }

    this.state = {
      open: false,
      exerciseInfo: exerciseInfo
    }

    this.handleClick = this.handleClick.bind(this);
    this.handleAddNewExerciseButtonClick = this.handleAddNewExerciseButtonClick.bind(this);
    this.handleRequestClose = this.handleRequestClose.bind(this);
    this.decrementWeight = this.decrementWeight.bind(this);
    this.incrementWeight = this.incrementWeight.bind(this);
    this.incrementSets = this.incrementSets.bind(this);
    this.decrementSets = this.decrementSets.bind(this);
    this.incrementReps = this.incrementReps.bind(this);
    this.decrementReps = this.decrementReps.bind(this);
    this.changeText = this.changeText.bind(this);
    this.editSets = this.editSets.bind(this);
    this.changeWeights = this.changeWeights.bind(this);
    this.exerciseChoosen = this.exerciseChoosen.bind(this);
    this.setExerciseText = this.setExerciseText.bind(this);
  }
  handleClick = (event) => {
    event.preventDefault();

    this.stateRedux = store.getState();

    let maxId = 0;
    for (let id in this.stateRedux.exercises) {
      if (Number(id) > maxId) maxId = id;
    }
    maxId++;

    let exerciseInfo = {
      title: 'Name of Exercise',
      id: maxId,
      sets: 3,
      reps: 3,
      history: [],
      increment: 2.5,
      weight: 10
    }

    this.setState({
      open: true,
      anchorEl: event.currentTarget,
      exerciseInfo: exerciseInfo
    });

  };
  handleAddNewExerciseButtonClick = () => {
    this.props.addExercise(this.state.exerciseInfo, this.props.workoutId);
  };
  handleRequestClose = () => {
    this.setState({
      open: false,
    });
  };
  decrementWeight() {
    let num = this.state.exerciseInfo.weight - this.state.exerciseInfo.increment;
    let nextExerciseInfo = dotProp.set(this.state.exerciseInfo, `weight`, num);
    this.setState({exerciseInfo: nextExerciseInfo});
  }
  incrementWeight() {
    let num = this.state.exerciseInfo.weight + this.state.exerciseInfo.increment;
    let nextExerciseInfo = dotProp.set(this.state.exerciseInfo, `weight`, num);
    this.setState({exerciseInfo: nextExerciseInfo});
  }
  decrementSets() {
    let num = Number(this.state.exerciseInfo.sets) - 1;
    let nextExerciseInfo = dotProp.set(this.state.exerciseInfo, `sets`, num);
    this.setState({exerciseInfo: nextExerciseInfo});
  }
  incrementSets() {
    let num = Number(this.state.exerciseInfo.sets) + 1;
    let nextExerciseInfo = dotProp.set(this.state.exerciseInfo, `sets`, num);
    this.setState({exerciseInfo: nextExerciseInfo});
  }
  decrementReps() {
    let num = Number(this.state.exerciseInfo.reps) - 1;
    let nextExerciseInfo = dotProp.set(this.state.exerciseInfo, `reps`, num);
    this.setState({exerciseInfo: nextExerciseInfo});
  }
  incrementReps() {
    let num = Number(this.state.exerciseInfo.reps) + 1;
    let nextExerciseInfo = dotProp.set(this.state.exerciseInfo, `reps`, num);
    this.setState({exerciseInfo: nextExerciseInfo});
  }
  changeText(e, val) {
    let nextExerciseInfo = dotProp.set(this.state.exerciseInfo, `title`, val);
    this.setState({exerciseInfo: nextExerciseInfo});
  }
  editSets(e, val) {
    let nextExerciseInfo = dotProp.set(this.state.exerciseInfo, `sets`, val);
    this.setState({exerciseInfo: nextExerciseInfo});
  }
  changeWeights(e, val) {
    let nextExerciseInfo = dotProp.set(this.state.exerciseInfo, `weights`, val);
    this.setState({exerciseInfo: nextExerciseInfo});
  }
  exerciseChoosen(chosenRequest, index) {
    this.setState({exerciseInfo: chosenRequest});
  }
  setExerciseText(exTest) {
    console.log(exTest);
    let exercise = Object.assign({}, this.state.exerciseInfo, {title: exTest});
    this.setState({exerciseInfo: exercise});
  }
  render() {
    const style = {
      display: 'inline-block',
      width: 50,
      minWidth: 50,
      margin: 5,
      textAlign: 'center'
    };
    const textAlignCenter = {
      textAlign: 'center'
    };

    console.log('rerendered');
    let state = store.getState();
    console.log(state.exercises);
    console.log(state.exercises[this.state.exerciseInfo.id]);

    const dataSource = Object.values(state.exercises);

    const dataSourceConfig = {
      text: 'title',
      value: 'id',
    };
    return (
      <div>
        <div style={{margin: '20px'}}>
          <a href="#" className="add-new-exercise-link" onClick={this.handleClick}>Add new exercise</a>
        </div>
        <div>
          <Dialog
            title={"Edit " + this.state.exerciseInfo.title}
            modal={false}
            open={this.state.open}
            autoScrollBodyContent={true}
            onRequestClose={this.handleRequestClose}
          >
            <div style={textAlignCenter}>
              <div>
                <h3>Name:</h3>
                  <AutoComplete
                    style={{ textAlign: 'center' }}
                    dataSource={dataSource}
                    openOnFocus={true}
                    filter={AutoComplete.caseInsensitiveFilter}
                    dataSourceConfig={dataSourceConfig}
                    onNewRequest={this.exerciseChoosen}
                    onChange={this.setExerciseText}
                  />
              </div>
              <div>
                <h3>Sets:</h3>
                <RaisedButton onClick={this.decrementSets} label="-" style={style} primary/>
                <TextField
                  inputStyle={{ textAlign: 'center' }}
                  value={this.state.exerciseInfo.sets}
                  style={style}
                  onChange={this.editSets}
                />
                <RaisedButton onClick={this.incrementSets} label="+" style={style} primary/>
              </div>
              <div>
                <h3>Reps:</h3>
                <RaisedButton onClick={this.decrementReps} label="-" style={style} primary/>
                <TextField
                  inputStyle={{ textAlign: 'center' }}
                  value={this.state.exerciseInfo.reps}
                  style={style}
                />
                <RaisedButton onClick={this.incrementReps} label="+" style={style} primary/>
              </div>
              <div>
                <h3>Weight:</h3>
                <RaisedButton onClick={this.decrementWeight} label="-" style={style} primary/>
                <TextField
                  inputStyle={{ textAlign: 'center' }}
                  value={this.state.exerciseInfo.weight}
                  style={style}
                  onChange={this.changeWeights}
                />
                <span>kg</span>
                <RaisedButton onClick={this.incrementWeight} label="+" style={style} primary/>
              </div>
              <div>
                <h4>Add New Exercise</h4>
                <RaisedButton onClick={this.handleAddNewExerciseButtonClick} label="Add" primary/>
              </div>
            </div>
          </Dialog>
        </div>
      </div>
    )
  }
}

export default connect(null, mapDispatchToProps)(EditRoutineWorkoutAddExercise);
