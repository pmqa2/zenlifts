import React from 'react';
import EditReps from '../elements/EditReps';
import EditSets from '../elements/EditSets';
import EditWeight from '../elements/EditWeight';
import EditRoutineWorkoutAddExercise from './EditRoutineWorkoutAddExercise';
import { SET_SETS } from '../../constants/actionTypes';
import { connect } from 'react-redux';
import EditExerciseDialogue from './EditExerciseDialogue';
import Dialog from 'material-ui/Dialog';
import Card from 'material-ui/Card';
import Typography from '@material-ui/core/Typography';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow'

class EditWeightContainer extends React.Component {
  constructor() {
    super();
    this.state = {
      open: false
    }
    this.handleClick = this.handleClick.bind(this);
    this.handleRequestClose = this.handleRequestClose.bind(this);
  }
  handleClick = (event) => {
    // This prevents ghost click.
    event.preventDefault();

    this.setState({
      open: true,
      anchorEl: event.currentTarget,
    });
  };

  handleRequestClose = () => {
    this.setState({
      open: false,
    });
  };
  render() {
    const popoverStyle = {
      padding: 20,
      width: 75
    };
    return (
      <TableRow className="exercise">
        <TableCell className="editable-div ex-name" onClick={this.handleClick}>
          <input type="text" readOnly placeholder={this.props.exerciseInfo.title}></input>
        </TableCell>
        <TableCell className="editable-div ex-sets" onClick={this.handleClick}>
          <span>{this.props.exerciseInfo.sets}</span>
          <span>x</span>
          <span>{this.props.exerciseInfo.reps}</span>
        </TableCell>
        <TableCell className="editable-div ex-weight">
          <div>
            <div className="weight-edit-lift">
              <span onClick={this.handleClick}>{this.props.exerciseInfo.weight} kg</span>
            </div>
            <Dialog
              title={"Edit " + this.props.exerciseInfo.title}
              modal={false}
              open={this.state.open}
              autoScrollBodyContent={true}
              onRequestClose={this.handleRequestClose}
            >
              <EditExerciseDialogue exerciseInfo={this.props.exerciseInfo} workoutId={false} />
            </Dialog>
          </div>
        </TableCell>
      </TableRow>
    )
  }
}

class EditRoutineWorkout extends React.Component {
  constructor() {
    super();
  }
  render() {
    const exerciseList = Object.keys(this.props.exercises).map((key, index) => {
     const exerciseInfo = this.props.exercises[key];
     return (
       <EditWeightContainer key = {index} exerciseInfo = {exerciseInfo}  />
     )
    });
    return (
      <Card className="workout-routine exercises-list">
        <Typography className="edit-routine-day-title">Exercises</Typography>
        <Table><TableBody>{exerciseList}</TableBody></Table>
      </Card>
    )
  }
}


class EditRoutine extends React.Component {
  constructor() {
    super();
  }
  render() {
      return (
        <EditRoutineWorkout
          exercises = {this.props.exercises}
        />
      )
  }
}

export default EditRoutine;
