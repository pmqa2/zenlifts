import React from 'react';
import { connect } from 'react-redux';
import { ADD_WORKOUT } from '../.././constants/actionTypes';

const mapStateToProps = state => ({ workout: state.workout });

const mapDispatchToProps = dispatch => ({
  addWorkout: (workout) =>
    dispatch({ type: ADD_WORKOUT, workout })
});

class AddNewWorkout extends React.Component {
  constructor() {
    super();
    this.state = {
      open: false,
    }

  }
  handleClick() {
    this.setState({open: !this.state.open });
  }
  render() {
    return (
      <div>
      <a href="#" onClick={this.handleClick.bind(this)}>Add new workout</a>
      <AddForm open={this.state.open} workouts={this.props.workout} addWorkout={this.props.addWorkout} />
      </div>
    )
  }
}

class AddForm extends React.Component {
  constructor() {
    super();
  }
  addWorkout(e) {
    let id = 0;
    this.props.workouts.map((i) => {
      (i.id);
      (id);
      if (i.id > id) id = i.id;
    })
    (this.props.workouts);
    // this.props.addWorkout({id: id++, title: e.target.value, exercises: []});
  }
  render() {
    if (this.props.open === false)
      return null;
    else {
      return (
        <div>
          <input type="text"></input>
          <button type="submit" onClick={(e) => this.addWorkout(e)}>Add</button>
        </div>
      )
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(AddNewWorkout);
