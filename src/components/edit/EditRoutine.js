import React from 'react';
import EditRoutineWorkout from './EditRoutineWorkout';
import Card from 'material-ui/Card';

class EditRoutine extends React.Component {
  constructor() {
    super();
  }
  render() {
    const routineList = this.props.routine.map(workout => {
      return (
        <EditRoutineWorkout
        key = {workout.id}
        workout = {workout}
        exerciseInfo = {this.props.exerciseInfo}
        handleWeightIncrementUpdate = {this.props.handleWeightIncrementUpdate}
        />
      )
    });
    return (
      <div className="text-center edit-routine">
        <div>{routineList}</div>
      </div>
    )
  }
}

export default EditRoutine;
