import React from 'react';
import EditReps from '../elements/EditReps';
import EditSets from '../elements/EditSets';
import EditWeight from '../elements/EditWeight';
import EditRoutineWorkoutAddExercise from './EditRoutineWorkoutAddExercise';
import { SET_SETS } from '../../constants/actionTypes';
import { connect } from 'react-redux';
import EditExerciseDialogue from './EditExerciseDialogue';
import Dialog from 'material-ui/Dialog';
import Card from 'material-ui/Card';
import Typography from '@material-ui/core/Typography';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow'


class EditWeightContainer extends React.Component {
  constructor() {
    super();
    this.state = {
      open: false
    }
    this.handleClick = this.handleClick.bind(this);
    this.handleRequestClose = this.handleRequestClose.bind(this);
  }
  handleClick = (event) => {
    // This prevents ghost click.
    event.preventDefault();

    this.setState({
      open: true,
      anchorEl: event.currentTarget,
    });
  };

  handleRequestClose = () => {
    this.setState({
      open: false,
    });
  };
  render() {
    const popoverStyle = {
      padding: 20,
      width: 75
    };
    return (
      <TableRow className="exercise">
        <TableCell className="editable-div ex-name" onClick={this.handleClick}>
          <input type="text" placeholder={this.props.exerciseInfo.title}></input>
        </TableCell>
        <TableCell className="editable-div ex-sets" onClick={this.handleClick}>
          <span>{this.props.exerciseInfo.sets}</span>
          <span>x</span>
          <span>{this.props.exerciseInfo.reps}</span>
        </TableCell>
        <TableCell className="editable-div ex-weight">
          <div>
            <div className="weight-edit-lift">
              <span onClick={this.handleClick}>{this.props.exerciseInfo.weight} kg</span>
            </div>
            <Dialog
              title={"Edit " + this.props.exerciseInfo.title}
              modal={false}
              open={this.state.open}
              autoScrollBodyContent={true}
              onRequestClose={this.handleRequestClose}
            >
              <EditExerciseDialogue exerciseInfo={this.props.exerciseInfo} workoutId={this.props.workoutId} />
            </Dialog>
          </div>
        </TableCell>
      </TableRow>
    )
  }
}

class EditRoutineWorkout extends React.Component {
  constructor() {
    super();
  }
  render() {
    const exerciseList = this.props.workout.exercises.map((exercise, index) => {
      const exerciseInfo = this.props.exerciseInfo[exercise.id];
      return (
        <EditWeightContainer key = {index} exerciseInfo = {exerciseInfo} workoutId={this.props.workout.id} />
      )
    });
    return (
      <div className="text-center edit-routine">
        <div>
          <Card className="workout-routine">
            <Typography className="edit-routine-day-title">{this.props.workout.title}</Typography>
            <Table><TableBody>{exerciseList}</TableBody></Table>
            <EditRoutineWorkoutAddExercise workoutId={this.props.workout.id}/>
          </Card>
        </div>
      </div>
    )
  }
}

export default EditRoutineWorkout;
