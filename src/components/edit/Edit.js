import React from 'react';
import Logo from '../elements/Logo';
import EditRoutine from './EditRoutine';
import { connect } from 'react-redux';
import defaultState from '../../workouts/State';
import AddNewWorkout from './LiftAddNewWorkout';
import { requests, helper } from '../../agent';
import {Tabs, Tab} from 'material-ui/Tabs';
import EditExercises from './EditExercises';
import Config from '../../config';

const mapStateToProps = state => ({ ...state });

class Edit extends React.Component {
  constructor() {
    super();
  }
  componentDidUpdate() {
    if (this.props.page !== 'login') {
      helper.saveOnAPI(this.props);
    }
  }
  render() {
    const containerStyles = {backgroundColor: 'rgb(72, 144, 226)'};
    return (
      <div className="text-center">
        {/*<AddNewWorkout /> */}
        <Tabs style={containerStyles}>
          <Tab label="Routines" style={{ overflow: 'initial' }}>
            <EditRoutine
              routine = {this.props.workout}
              exerciseInfo = {this.props.exercises}
              handleWeightIncrementUpdate = {this.handleWeightIncrementUpdate}
            />
          </Tab>
          <Tab label="Exercises" style={{ overflow: 'initial' }}>
            <EditExercises
              exercises = {this.props.exercises}
            />
          </Tab>
        </Tabs>
      </div>
    )
  }
}

export default connect(mapStateToProps)(Edit);
