import React from 'react';
import { SET_WEIGHT, SET_NAME, SET_SETS, SET_REPS, DELETE_EXERCISE, DELETE_EXERCISE_COMPLETELY } from '../../constants/actionTypes';
import { connect } from 'react-redux';
import Dialog from 'material-ui/Dialog';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import NumberFormat from 'react-number-format';

const mapDispatchToProps = dispatch => ({
  onChangeWeight: (weight, id) =>
    dispatch({ type: SET_WEIGHT, weight, id }),
  onChangeExerciseName: (name, id) =>
    dispatch({ type: SET_NAME, name, id }),
  onChangeSets: (sets, id) =>
    dispatch({ type: SET_SETS, sets, id }),
  onChangeReps: (reps, id) =>
    dispatch({ type: SET_REPS, reps, id }),
  deleteExerciseWorkout: (id, workoutId) =>
    dispatch({ type: DELETE_EXERCISE, id, workoutId }),
  deleteExerciseCompletely: (id) =>
    dispatch({ type: DELETE_EXERCISE_COMPLETELY, id })
});

class DialogEditExercise extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      weight: this.props.exerciseInfo.weight
    }
    this.decrementWeight = this.decrementWeight.bind(this);
    this.incrementWeight = this.incrementWeight.bind(this);
    this.incrementSets = this.incrementSets.bind(this);
    this.decrementSets = this.decrementSets.bind(this);
    this.incrementReps = this.incrementReps.bind(this);
    this.decrementReps = this.decrementReps.bind(this);
    this.changeText = this.changeText.bind(this);
    this.editSets = this.editSets.bind(this);
    this.editReps = this.editReps.bind(this);
    this.changeWeights = this.changeWeights.bind(this);
    this.deleteExercise = this.deleteExercise.bind(this);
    this.cleanNumber = this.cleanNumber.bind(this);
    this.changeWeightsState = this.changeWeightsState.bind(this);
  }
  cleanNumber(n, typey = 'integer') {
    if (typeof(n) === 'number') {
      n = Math.abs(Number(n));
    } else {
      n = n.replace(/[^0-9.]/g,"");
      n = Math.abs(Number(n));
    };

    console.log(typeof(n));
    console.log(n);

    // if (typey === 'partial') {
    //   console.log(n);
    //   n = n.toFixed(2);
    // } else if (typey === 'integer') {
    //   n = Math.floor(n);
    // };

    return n;
  }
  decrementWeight() {
    let num = this.props.exerciseInfo.weight - this.props.exerciseInfo.increment;
    this.props.onChangeWeight(this.cleanNumber(num, 'partial'), this.props.exerciseInfo.id);
    this.setState({weight: this.cleanNumber(num, 'partial')});
  }
  incrementWeight() {
    let num = this.props.exerciseInfo.weight + this.props.exerciseInfo.increment;
    this.props.onChangeWeight(this.cleanNumber(num), this.props.exerciseInfo.id);
    this.setState({weight: this.cleanNumber(num, 'partial')});
  }
  decrementSets() {
    let num = Number(this.props.exerciseInfo.sets) - 1;
    this.props.onChangeSets(this.cleanNumber(num), this.props.exerciseInfo.id);
  }
  incrementSets() {
    let num = Number(this.props.exerciseInfo.sets) + 1;
    this.props.onChangeSets(this.cleanNumber(num), this.props.exerciseInfo.id);
  }
  decrementReps() {
    let num = Number(this.props.exerciseInfo.reps) - 1;
    this.props.onChangeReps(this.cleanNumber(num), this.props.exerciseInfo.id);
  }
  incrementReps() {
    let num = Number(this.props.exerciseInfo.reps) + 1;
    this.props.onChangeReps(this.cleanNumber(num), this.props.exerciseInfo.id);
  }
  changeText(e, val) {
    this.props.onChangeExerciseName(val, this.props.exerciseInfo.id)
  }
  editSets(e, val) {
    this.props.onChangeSets(this.cleanNumber(val), this.props.exerciseInfo.id);
  }
  editReps(e, val) {
    this.props.onChangeReps(this.cleanNumber(val), this.props.exerciseInfo.id);
  }
  changeWeights(e, val) {
    this.props.onChangeWeight(this.cleanNumber(this.state.weight, 'partial'), this.props.exerciseInfo.id);
  }
  changeWeightsState(e, val) {
    this.setState({ weight: val });
  }
  deleteExercise() {
    if (this.props.workoutId === false)
      this.props.deleteExerciseCompletely(this.props.exerciseInfo.id);
    else
      this.props.deleteExerciseWorkout(this.props.exerciseInfo.id, this.props.workoutId);
  }
  render() {
    const style = {
      display: 'inline-block',
      width: 50,
      minWidth: 50,
      margin: 5,
      textAlign: 'center'
    };
    const textAlignCenter = {
      textAlign: 'center'
    };
    let weight = this.props.exerciseInfo.weight.toString();
    console.log(typeof(weight));

    return (
      <div style={textAlignCenter}>
        <div>
          <h3>Name:</h3>
          <TextField
            inputStyle={{ textAlign: 'center' }}
            value={this.props.exerciseInfo.title}
            onChange={this.changeText}
            fullWidth={true}
          />
        </div>
        <div>
          <h3>Sets:</h3>
          <RaisedButton onClick={this.decrementSets} label="-" style={style} primary/>
          <TextField
            inputStyle={{ textAlign: 'center' }}
            value={this.props.exerciseInfo.sets}
            style={style}
            onChange={this.editSets}
          />
          <RaisedButton onClick={this.incrementSets} label="+" style={style} primary/>
        </div>
        <div>
          <h3>Reps:</h3>
          <RaisedButton onClick={this.decrementReps} label="-" style={style} primary/>
          <TextField
            inputStyle={{ textAlign: 'center' }}
            value={this.props.exerciseInfo.reps}
            style={style}
            onChange={this.editReps}
          />
          <RaisedButton onClick={this.incrementReps} label="+" style={style} primary/>
        </div>
        <div>
          <h3>Weight:</h3>
          <RaisedButton onClick={this.decrementWeight} label="-" style={style} primary/>
          <TextField
            inputStyle={{ textAlign: 'center' }}
            value={this.state.weight}
            style={style}
            onChange={this.changeWeightsState}
            onBlur={this.changeWeights}
          />
          <span>kg</span>
          <RaisedButton onClick={this.incrementWeight} label="+" style={style} primary/>
        </div>
        <div>
          <h4>Delete Exercise</h4>
          <RaisedButton onClick={this.deleteExercise} label="Delete" secondary/>
        </div>
      </div>
    )
  }
}

export default connect(null, mapDispatchToProps)(DialogEditExercise);
