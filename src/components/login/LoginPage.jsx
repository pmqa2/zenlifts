import React, { PropTypes } from 'react';
import { Auth } from '../../agent';
import LoginForm from '../login-components/LoginForm.jsx';
import Config from '../../config';
import { LOAD_APP } from '../../constants/actionTypes';
import { connect } from 'react-redux';
import { requests } from '../../agent';
import Logo from '../elements/Logo';
import RaisedButton from 'material-ui/RaisedButton';
import State from '../../workouts/State';
import LoadingGif from '../../assets/loading.gif';

const mapDispatchToProps = dispatch => ({
  onLoad: (state) => {
    dispatch({ type: LOAD_APP, state })
  }
});

const mapStateToProps = state => ({ ...state });

class LoginPage extends React.Component {

  /**
   * Class constructor.
   */
  constructor(props, context) {
    super(props, context);

    const storedMessage = localStorage.getItem('successMessage');
    let successMessage = '';

    if (storedMessage) {
      successMessage = storedMessage;
      localStorage.removeItem('successMessage');
    }

    // set the initial component state
    this.state = {
      errors: {},
      loading: false,
      successMessage,
      user: {
        email: '',
        password: ''
      }
    };

    this.processForm = this.processForm.bind(this);
    this.changeUser = this.changeUser.bind(this);
  }

  /**
   * Process the form.
   *
   * @param {object} event - the JavaScript event object
   */
  processForm(event, reset) {
    // prevent default action. in this case, action is the form submission event
    event.preventDefault();

    //window.location = '/#';

    // create a string for an HTTP body message
    const email = encodeURIComponent(this.state.user.email);
    const password = encodeURIComponent(this.state.user.password);
    const formData = `email=${email}&password=${password}`;

    // create an AJAX request
    const xhr = new XMLHttpRequest();
    xhr.open('post', `${Config.api}/auth/login`);
    this.setState({ loading: true });
    xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    xhr.responseType = 'json';
    xhr.addEventListener('load', () => {
      if (xhr.status === 200) {
        // success

        // change the component-container state
        this.setState({
          errors: {}
        });

        // save the token
        Auth.authenticateUser(xhr.response.token);

        // get the user data

        // change the current URL to /
        //window.location.href = '/#';

        //(reset);

        if (reset === true) {
          // localStorage.setItem('data', JSON.stringify(State));
          // (State);
          // const dataObj = JSON.stringify(State);
          // requests.post('api/data', 'data=' + dataObj).then((err, res) => {
          //   (err);
          //   (res);
          // });
        } else {
          requests.get('api/data').then((res, err) => {
            if (err) {
              return null;
            }
            console.log(res);
            res.data.page = 'home';
            console.log(res);
            this.props.onLoad(res.data);
          });
        }
      } else {
        // failure

        // change the component state
        const errors = xhr.response.errors ? xhr.response.errors : {};
        errors.summary = xhr.response.message;

        this.setState({
          errors: errors,
          loading: false
        });
      }
    });
    (JSON.stringify(formData));
    xhr.send(formData);
  }

  /**
   * Change the user object.
   *
   * @param {object} event - the JavaScript event object
   */
  changeUser(event) {
    const field = event.target.name;
    const user = this.state.user;
    user[field] = event.target.value;

    this.setState({
      user
    });
  }

  /**
   * Render the component.
   */
  render() {
    console.log(this.state);
    if (!this.state.loading) {
      return (
        <div className="login-page">
          <Logo />
          <LoginForm
            onSubmit={this.processForm}
            onChange={this.changeUser}
            errors={this.state.errors}
            successMessage={this.state.successMessage}
            user={this.state.user}
            resetDataLogin={(e)=>this.processForm(e, true)}
          />
        </div>
      );
    } else {
        let verticalAlignStyle = { marginTop: window.innerHeight/2-30 };
          return (
            <div style={verticalAlignStyle} className="text-center loading-gif">
              <img src={LoadingGif} />
            </div>
          );
      }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginPage);
