import React from 'react';
import Logo from '../elements/Logo';
import { connect } from 'react-redux';
import { SET_WORKOUT, GO_TO_PAGE, LOAD_APP } from '../.././constants/actionTypes';
import setupWorkoutHelper from '../../workouts/index.js';
import { store } from '../../store';
import { Auth } from '../../agent';
import { requests, helper } from '../../agent';
import State from '../../workouts/State';
import Button from 'material-ui/RaisedButton';
import Config from '../../config';

const mapStateToProps = state => ({ ...state });

const mapDispatchToProps = dispatch => ({
  setWorkout: (payload) =>
    dispatch({ type: SET_WORKOUT, payload }),
  goToPage: (payload) =>
    dispatch({ type: GO_TO_PAGE, payload }),
  loadApp: (state) => {
    dispatch({ type: LOAD_APP, state })
  }
});

class Home extends React.Component {
  constructor() {
    super();
    this.setupWorkout = this.setupWorkout.bind(this);
    this.downloadDataFromServer = this.downloadDataFromServer.bind(this);
    this.uploadDataToServer = this.uploadDataToServer.bind(this);
    this.stateRedux = store.getState();
  }
  componentWillMount() {
    this.uploadDataToServer();
  }
  componentDidUpdate() {
    ('home component updated');
    this.uploadDataToServer();
  }
  downloadDataFromServer() {
    requests.get('api/data').then((res, err) => {
      if (err) return null;

      res.data.page = 'home';
      this.props.loadApp(res.data);
    });
  }
  uploadDataToServer() {
    helper.saveOnAPI(this.stateRedux);
  }
  deauthenticate() {
    Auth.deauthenticateUser();
    this.props.loadApp({page: 'login'});
  }
  resetState() {
    this.props.loadApp(State);
    localStorage.setItem(Config.dataToken, JSON.stringify(State));
    requests.post('api/data', 'data=' + JSON.stringify(State)).then((err, res) => {
      console.log(err);
      console.log(res);
    });
  }
  setupWorkout() {
    let currentWorkout, selectedWorkout = {};

    const nextWorkout = setupWorkoutHelper(this.stateRedux);

    // if there is already workout use that one
    if (this.props.currentWorkout !== false) {
      currentWorkout = this.props.currentWorkout;
    }
    else {
      if (this.props.workoutHistory.length > 0) {
        let id = this.props.workoutHistory[this.props.workoutHistory.length-1].id + 1;
        let found = false;

        this.props.workout.map(wk => {
          if (wk.id === id) {
            found = wk;
          }
        });

        if (found === false) {
          selectedWorkout = this.props.workout[0];
        } else selectedWorkout = found;

      } else {
        selectedWorkout = this.props.workout[0];
      }
      const exerciseInfo = this.props.exercises;

      currentWorkout = {
        id: selectedWorkout.id,
        title:  selectedWorkout.title,
        exercises: selectedWorkout.exercises.map(exercise => {

          return {
            id: exercise.id,
            title: exerciseInfo[exercise.id].title,
            weight: exerciseInfo[exercise.id].weight,
            warmup: false,
            completed: false,
            sets: (function() {
              let arrSets = [];
              for (let i = 0; i < exerciseInfo[exercise.id].sets; i++ ) {
                arrSets.push({
                  id: i,
                  totalReps: exerciseInfo[exercise.id].reps,
                  clickedReps: false,
                  completed: false
                });
              }
              return arrSets;
            })()
          }

        })
      }
    }
    this.props.setWorkout(currentWorkout)
  }

  render() {
    return (
      <div className="text-center home">
        <Logo />
        <h2 onClick={this.setupWorkout}>Lift now.</h2>
        <h2 onClick={() => this.props.goToPage('edit')}>Edit Workout</h2>
        <h2 onClick={() => this.deauthenticate() }>Logout</h2>
      </div>
    )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);
