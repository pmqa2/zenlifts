import superagentPromise from 'superagent-promise';
import _superagent from 'superagent';
import Config from './config';
import axios from 'axios';
import { createStore, applyMiddleware } from 'redux'

const superagent = superagentPromise(_superagent, global.Promise);
const encode = encodeURIComponent;
const responseBody = res => res.body;

const Auth = {
  authenticateUser: (token) => {
    localStorage.setItem(Config.connectionToken, token);
  },
  isUserAuthenticated: () => {
    return localStorage.getItem(Config.connectionToken) !== null;
  },
  deauthenticateUser: () => {
    localStorage.removeItem(Config.connectionToken);
  },
  getToken: () => {
    return localStorage.getItem(Config.connectionToken);
  }
};

let token = null;

const tokenPlugin = req => {

  if (Auth.isUserAuthenticated()) {
    token = Auth.getToken();
  }
  if (token) {
    req.set('authorization', `Token ${token}`);
  }
}

const requests = {
  del: url =>
    superagent.del(`${Config.api}/${url}`).use(tokenPlugin).then(responseBody),
  get: (url, queryData) =>
    superagent.get(`${Config.api}/${url}`).query(queryData).use(tokenPlugin).then(responseBody),
  put: (url, body) =>
    superagent.put(`${Config.api}/${url}`, body).use(tokenPlugin).then(responseBody),
  post: (url, body) =>
    superagent.post(`${Config.api}/${url}`, body).use(tokenPlugin).then(responseBody)
};

const requestsAxios = axios.create({
  baseURL: Config.api,
  timeout: 1000,
  headers: { Authorization: "Bearer " + Auth.getToken() }
});

const helper = {
  saveOnAPI: state => {
    let nextState = Object.assign({}, state);
    nextState.timestamp = Date.now();
    localStorage.setItem(Config.dataToken, JSON.stringify(nextState));
    const dataObj = JSON.stringify(nextState);
    requests.post('api/data', 'data=' + dataObj).then((err, res) => {
      (err);
      (res);
    });
  },
  saveOffline: state => {
    let nextState = Object.assign({}, state);
    nextState.timestamp = Date.now();
    localStorage.setItem(Config.dataToken, JSON.stringify(nextState));
  }
}

export {
  Auth,
  requests,
  requestsAxios,
  helper
};
