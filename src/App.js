import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import ReactDOM from 'react-dom';
import Timer from './components/lift/LiftTimer';
import Home from './components/home/Home';
import Lift from './components/lift/Lift';
import Edit from './components/edit/Edit';
import { Provider } from 'react-redux';
import { store } from './store';
import { LOAD_APP } from './constants/actionTypes';
import { connect } from 'react-redux';
import State from './workouts/State';
import { Auth } from './agent';
import Login from './components/login/LoginPage';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import { requests, requestsAxios } from './agent';
import Header from './components/header/Header';
import LoadingGif from './assets/loading.gif';
import Config from './config';

const mapDispatchToProps = dispatch => ({
  onLoad: (state) => {
    dispatch({ type: LOAD_APP, state })
  }
});

const mapStateToProps = state => ({ ...state });

class App extends Component {
  constructor() {
    super();
    this.loadApp = this.loadApp.bind(this);
  }
  componentDidMount() {
    this.loadApp();
  }
  loadApp() {
    const userIsAuthenticated = Auth.isUserAuthenticated();

    if (userIsAuthenticated) {

      requestsAxios.get('api/data')
      .then((res) => {
        if (localStorage.getItem('zenliftsData')) {
          let dataObj = JSON.parse(localStorage.getItem('zenliftsData'));
          if (dataObj.timestamp > res.data.data.timestamp) {
            this.props.onLoad(dataObj);
          } else {
            this.props.onLoad(res.data.data);
            localStorage.setItem('zenliftsData', JSON.stringify(res.data.data));
          }
        } else {
          this.props.onLoad(res.data.data);
          localStorage.setItem('zenliftsData', JSON.stringify(res.data.data));
        }
      })
      .catch((error) => {
        let data = localStorage.getItem('zenliftsData');
        if (data) {
          this.props.onLoad(JSON.parse(data));
        } else {
          this.props.onLoad({ page: 'login' });
        }
      });

    } else {
      this.props.onLoad({ page: 'login' });
    }
  }
  render() {
    if (this.props.page) {
      switch (this.props.page) {
        case 'home':
          return (
            <Home />
          )
          break;
        case 'edit':
          return (
            <div className="wrapper">
              <div className="container">
                <Header pageTitle="Edit Routine" />
              </div>
              <div className="container">
                <Edit />
              </div>
            </div>
          )
          break;
        case 'workout':
          return (
            <div className="wrapper">
              <div className="container">
                <Header pageTitle="Lift" />
              </div>
              <div className="container">
                <Lift />
              </div>
            </div>
          );
          break;
          default:
        case 'login':
          return (
            <div className="wrapper">
              <MuiThemeProvider muiTheme={getMuiTheme()}>
                <Login />
              </MuiThemeProvider>
            </div>
          )
      }
    } else {
      let verticalAlignStyle = { marginTop: window.innerHeight/2-30 };
        return (
          <div style={verticalAlignStyle} className="text-center loading-gif">
            <img src={LoadingGif} />
          </div>
        )
      }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
