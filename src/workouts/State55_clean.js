let State = {
page: 'home',
timer: {
  resetTimer: false,
  totalSeconds: false,
  timerString: ''
},
currentWorkout: false,
"exercises": {
  "1": {
    "id": 1,
    "title": "Bench Press",
    "sets": 5,
    "reps": 5,
    "weight": 50,
    "increment": 2.5,
    "history": []
  },
  "2": {
    "id": 2,
    "title": "Deadlifts",
    "sets": 1,
    "reps": 5,
    "weight": 50,
    "increment": 2.5,
    "history": []
  },
  "3": {
    "id": 3,
    "title": "Squat",
    "sets": 5,
    "reps": 5,
    "weight": 40,
    "increment": 2.5,
    "history": []
  },
  "4": {
    "id": 4,
    "title": "Overhead Press",
    "sets": 5,
    "reps": 5,
    "weight": 25,
    "increment": 2.5,
    "history": []
  },
  "5": {
    "id": 5,
    "title": "Barbell Row",
    "sets": 5,
    "reps": 5,
    "weight": 40,
    "increment": 2.5,
    "history": []
  },
  "6": {
    "id": 6,
    "title": "Barbell Shrug",
    "sets": 4,
    "reps": 16,
    "weight": 5,
    "increment": 2.5,
    "history": []
  },
  "7": {
    "id": 7,
    "title": "Neck Curl",
    "sets": 4,
    "reps": 16,
    "weight": 5,
    "increment": 0.5,
    "history": []
  },
  "11": {
    "id": 11,
    "title": "Skull Crushers",
    "sets": 4,
    "reps": 16,
    "weight": 10,
    "increment": 2.5,
    "history": []
  },
  "12": {
    "id": 12,
    "title": "Lateral Raises (Superset)",
    "sets": 3,
    "reps": 16,
    "weight": 16,
    "increment": 2.5,
    "history": []
  },
  "13": {
    "id": 13,
    "title": "Barbell Shrug",
    "sets": 4,
    "reps": 16,
    "weight": 40,
    "increment": 2.5,
    "history": []
  },
  "15": {
    "id": 15,
    "title": "Hammer Curl",
    "sets": 4,
    "reps": 16,
    "weight": 10,
    "increment": 2.5,
    "history": []
  },
  "16": {
    "id": 16,
    "title": "Dumbbell Curl",
    "sets": 4,
    "reps": 16,
    "weight": 10,
    "increment": 2.5,
    "history": []
  }
},
workout: [
  {
    id: 1,
    title: 'Push',
    exercises: [{ id: 1}, {id: 11}, {id: 12}]
  },
  {
    id: 2,
    title: 'Pull',
    exercises: [{ id: 2}, {id: 13}, {id: 7}]
  },
  {
    id: 3,
    title: 'Legs',
    exercises: [{ id: 3}, {id: 16}, {id: 12}]
  },
  {
    id: 4,
    title: 'Push',
    exercises: [{ id: 4}, {id: 13}, {id: 7}]
  },
  {
    id: 5,
    title: 'Pull',
    exercises: [{ id: 5}, {id: 15}, {id: 12}]
  },
  {
    id: 6,
    title: 'Legs',
    exercises: [{ id: 3}, {id: 13}, {id: 7}]
  }
],
workoutHistory: []
};
export default State;
