let State = {
page: 'home',
timer: {
  resetTimer: false,
  totalSeconds: false,
  timerString: ''
},
currentWorkout: false,
exercises: {
  "1": {
    id: 1,
    title: 'Bench Press',
    sets: 5,
    reps: 5,
    weight: 25,
    increment: 2.5,
    history: []
  },
  "6": {
    id: 6,
    title: 'Lateral Raises',
    sets: 5,
    reps: 12,
    weight: 5,
    increment: 0.5,
    history: []
  },
  "7": {
    id: 7,
    title: 'Front Dumbell raises',
    sets: 5,
    reps: 12,
    weight: 5,
    increment: 0.5,
    history: []
  },
  "10": {
    id: 10,
    title: "Barbell Curl",
    sets: 5,
    reps: 12,
    weight: 10,
    increment: 2.5,
    history: []
  },
  "11": {
    id: 11,
    title: "Close-grip Bench Press",
    sets: 5,
    reps: 12,
    weight: 10,
    increment: 2.5,
    history: []
  },
  "12": {
    id: 12,
    title: "Neck Curl",
    sets: 5,
    reps: 12,
    weight: 5,
    increment: 2.5,
    history: []
  },
  "13": {
    id: 13,
    title: "Forearm Curl",
    sets: 5,
    reps: 12,
    weight: 5,
    increment: 2.5,
    history: []
  },
  "14": {
    id: 14,
    title: "Dumbbel Shrugs",
    sets: 5,
    reps: 12,
    weight: 5,
    increment: 2.5,
    history: []
  }
},
workout: [
  {
    id: 1,
    title: 'Brolifts',
    exercises: [{id: 11}, {id: 10}, {id: 6}, {id: 7}, {id: 12}, {id: 13}, {id: 14}]
  }
],
workoutHistory: []
};
export default State;
