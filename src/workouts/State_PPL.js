let State = {
page: 'home',
timer: {
  resetTimer: false,
  totalSeconds: false,
  timerString: ''
},
currentWorkout: false,
"exercises": {
  "1": {
    "id": 1,
    "title": "Bench Press",
    "sets": 3,
    "reps": 3,
    "weight": 50,
    "increment": 2.5,
    "history": []
  },
  "21": {
    "id": 21,
    "title": "Bench Press (Bodybuilding)",
    "sets": 4,
    "reps": 8,
    "weight": 40,
    "increment": 2.5,
    "history": []
  },
  "2": {
    "id": 2,
    "title": "Deadlifts",
    "sets": 3,
    "reps": 3,
    "weight": 50,
    "increment": 2.5,
    "history": []
  },
  "22": {
    "id": 22,
    "title": "Deadlifts (Bodybuilding)",
    "sets": 3,
    "reps": 8,
    "weight": 40,
    "increment": 2.5,
    "history": []
  },
  "3": {
    "id": 3,
    "title": "Squat",
    "sets": 3,
    "reps": 3,
    "weight": 40,
    "increment": 2.5,
    "history": []
  },
  "23": {
    "id": 23,
    "title": "Squat (Bodybuilding)",
    "sets": 3,
    "reps": 8,
    "weight": 40,
    "increment": 2.5,
    "history": []
  },
  "4": {
    "id": 4,
    "title": "Overhead Press",
    "sets": 3,
    "reps": 3,
    "weight": 25,
    "increment": 2.5,
    "history": []
  },
  "24": {
    "id": 24,
    "title": "Overhead Press (Bodybuilding)",
    "sets": 4,
    "reps": 8,
    "weight": 25,
    "increment": 2.5,
    "history": []
  },
  "5": {
    "id": 5,
    "title": "Barbell Row",
    "sets": 3,
    "reps": 3,
    "weight": 40,
    "increment": 2.5,
    "history": []
  },
  "25": {
    "id": 25,
    "title": "Barbell Row (Bodybuilding)",
    "sets": 4,
    "reps": 8,
    "weight": 40,
    "increment": 2.5,
    "history": []
  },
  "6": {
    "id": 6,
    "title": "Barbell Shrug",
    "sets": 4,
    "reps": 8,
    "weight": 5,
    "increment": 2.5,
    "history": []
  },
  "7": {
    "id": 7,
    "title": "Neck Curl",
    "sets": 4,
    "reps": 12,
    "weight": 5,
    "increment": 0.5,
    "history": []
  },
  "11": {
    "id": 11,
    "title": "Skull Crushers",
    "sets": 4,
    "reps": 8,
    "weight": 10,
    "increment": 2.5,
    "history": []
  },
  "12": {
    "id": 12,
    "title": "Lateral Raises (Superset)",
    "sets": 3,
    "reps": 16,
    "weight": 16,
    "increment": 2.5,
    "history": []
  },
  "13": {
    "id": 13,
    "title": "Barbell Shrug",
    "sets": 4,
    "reps": 12,
    "weight": 40,
    "increment": 2.5,
    "history": []
  },
  "15": {
    "id": 15,
    "title": "Hammer Curl",
    "sets": 3,
    "reps": 1,
    "weight": 10,
    "increment": 2.5,
    "history": []
  },
  "16": {
    "id": 16,
    "title": "Dumbbell Curl",
    "sets": 3,
    "reps": 1,
    "weight": 10,
    "increment": 2.5,
    "history": []
  }
},
workout: [
  {
    id: 1,
    title: 'Push',
    exercises: [{ id: 1}, {id: 21}]
  },
  {
    id: 2,
    title: 'Pull',
    exercises: [{ id: 2}, {id: 22}]
  },
  {
    id: 3,
    title: 'Legs',
    exercises: [{ id: 3}, {id: 23}]
  },
  {
    id: 4,
    title: 'Push',
    exercises: [{ id: 4}, {id: 24}]
  },
  {
    id: 5,
    title: 'Pull',
    exercises: [{ id: 5}, {id: 25}]
  },
  {
    id: 6,
    title: 'Legs',
    exercises: [{ id: 3}, {id: 23}]
  }
],
workoutHistory: []
};
export default State;
